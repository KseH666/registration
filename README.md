# Система регистрации и учёта пациентов.

Данный дипломный проект создан и защищён в ходе курса __"Введение в разработку корпоративных приложений на Java"__ от __АНО ВО "Университет Иннополис"__.

Проект представляет собой __Spring MVC__ приложение для больничной регистрации и учёта пациентов. В ходе написания проекта были изучены особенности использования __Spring Framework__, в частности __Spring MVC__. Данное веб-приложение позволяет регистрироваться пользователям, добавлять врачей и пациентов, а также редактировать их анкеты и производить движение пациентов в ходе их лечения.

__Стек технологий:__
- _Spring Boot;_
- _Spring Security;_
- _Spring Data JPA, Hibernate;_
- _PostgreSQL;_
- _HTML, FreeMarker._

В дальнейшем планируется поддерживать проект для изучения применения рабочих технологий на практике. 

В проект будут добавлены:

 - [x] Профиль пользователя с возможностью редактирования информации.
 - [x] Расширенные функции у пользователя с ролью _ADMIN_.
 - [ ] Сортировки в таблицах.
 - [x] Пагинация списка пациентов и врачей.

### Скриншоты работы приложения:
#### 1) Структура базы данных:
![](https://sun9-54.userapi.com/impg/SS66XGNFebJNtzwAvcdFcSZqCHe-R92hf9gIMg/esk-8rA6HNs.jpg?size=813x621&quality=96&sign=87a01825d70194cbc6a726b4ef34c126&type=album)
#### 2) Стартовая страница index.html:
![](https://sun9-84.userapi.com/impg/4JA7C8NCvmT-1_tDWxgJJKP3s8xtwBJhBe0uXQ/CdTMoMGseZU.jpg?size=1918x1039&quality=96&sign=74751f6b6d0b4a5b0001a7fb4ec03c3c&type=album)
#### 3) Страница регистрации:
![](https://sun9-85.userapi.com/impg/rKZYpo87QwZgaTlXo5ZxJ16Os6T5Ie6YG0HDOg/PDxGB3tX_uk.jpg?size=466x412&quality=96&sign=e58f3e1a059b66a2b2320c2d29a5c026&type=album)
#### 4) Страница входа:
![](https://sun9-29.userapi.com/impg/zpJiVVdquwsqy_QHE0jPtgf9nlSqbW1UxyfaOg/FegyruOINzQ.jpg?size=382x293&quality=96&sign=10f1f7c33513818d3a3ac4845fbebbd0&type=album)
#### 5) Главная страница:
![](https://sun9-77.userapi.com/impg/O87MevgOYHcW-6ZxaHeiITgv3RSlpTp53JDwow/GrvHB4a-tA0.jpg?size=474x462&quality=96&sign=49b5fd92f36be9781a26324c4a096309&type=album)
#### 6) Страница со списком пациентов:
![](https://sun9-7.userapi.com/impg/e1oAjYIAnvftPXYjHcsj8XLvoUG72VT7KfMkLg/Cigt3a4M2WI.jpg?size=852x927&quality=96&sign=472791277a43d99ae4dd8efbe8ab7d90&type=album)
#### 7) Страница редактирования анкеты пациента:
![](https://sun9-18.userapi.com/impg/_EGhbUxp5XrmmwLSufXnYGyEDMxOBneTg4A2gQ/eq2tDXFxb3Y.jpg?size=1893x1427&quality=96&sign=21b226ef37ae4584c85b694c425d775d&type=album)
#### 8) Страница со списком врачей:
![](https://sun9-30.userapi.com/impg/vRYBsAa2Uz2Ogcmkks3POg-cXpIwaIKXC616yQ/wQPLaw15on8.jpg?size=869x807&quality=96&sign=1324aaf0d39074227616fbac1c47a646&type=album)
#### 9) Страница редактирования анкеты врача:
![](https://sun9-48.userapi.com/impg/zvE1Jj862P2OoHZWJQA0sFNXIQhi6QnNGPi7Mw/Sf2MN4zWnGs.jpg?size=1372x796&quality=96&sign=5b44df4f8e801040aade736cd5b2c3c8&type=album)
#### 10) Страница со списком пациентов конкретного врача:
![](https://sun9-23.userapi.com/impg/KvJgwK-v6QB2p3FNA19anUjTfwztUD5XIxdSkQ/BF4xaccU8Hs.jpg?size=1757x785&quality=96&sign=5795f14124cc2b256a14fa8c19df2305&type=album)
#### 11) Страница информации о профиле:
![](https://sun9-19.userapi.com/impg/tS4FO1oFzKnHBQfbwJBiIbGV7LGzLKEr7-kIJg/3lIhG3t34Hk.jpg?size=988x440&quality=96&sign=a7d03b24ccecd590e0f3e29392a06fc2&type=album)
#### 12) Страница со списком профилей:
![](https://sun9-73.userapi.com/impg/6tZL1EngX08zMUkoLNNuhN8MOtMYSn8Xrywncw/fyT3v19DfoQ.jpg?size=1489x536&quality=96&sign=cacd37b3edd6ca5287ee6c4dc12b0bb0&type=album)
#### 13) Страница с редактиврованием профиля:
![](https://sun9-60.userapi.com/impg/FV8PEIlPmRA87cb0iJs3DAdS8PcGMB7EQXPBxQ/M76-ef9gz6M.jpg?size=1570x646&quality=96&sign=c783c54b27c631abd6395cb4447cb952&type=album)
#### 14) Все страницы с POST-запросами имеют валидацию форм похожую на данную:
![](https://sun9-42.userapi.com/impg/LP9rw6af5l1JIOjbt3b-rRRcvtTjazuKefsahQ/fiVGLlXaaig.jpg?size=618x169&quality=96&sign=dc5cb91076e912ab6998b8ab598bcbb3&type=album)
#### 15) Страница с ошибкой 404:
![](https://sun9-24.userapi.com/impg/5kZyFWZVjvzjKZKpGxvOV_ro9gESZxUI4AlsGg/Xy2MTBzPbS4.jpg?size=1211x899&quality=96&sign=e2c56ea59b1c4961c1cdfd33577e396e&type=album)
#### 16) Если пользователь авторизован, но не верифицирован, то у него нет доступа к POST-запросам. При этом блокируются кнопки для отправки форм:
![](https://sun9-49.userapi.com/impg/WxDJGTNbcKAuaH_Zs5O8bIHqGkLJ5qt7zs3zEA/Rq7aVI8e0mk.jpg?size=647x272&quality=96&sign=3fcc0d4d74b241866bfa15e305c588aa&type=album)

Также на каждой странице есть простенькие анимации сделанные на CSS.

###### А это я погружаюсь в Java...
![](https://psv4.userapi.com/c856224/u203199838/docs/d5/13126f3bc97f/ezgif-7-98918cb7db02.gif?extra=99xpVFkzTTxWC38X08erDu-i0U8DWPU_Sb5_mxJcavMv7mq8xo3ovNU5_IfFjlNph_dTC3dMdmQhHEXehTm8Y2AZCn0IdpveKelTYwLaznwb6Xy_U4EfzXUOBDKkYZpz1AUCB6VPgVKUcQ2YMtReDg)

package hospital.registration.controllers;

import hospital.registration.forms.DoctorForm;
import hospital.registration.services.DoctorService;
import hospital.registration.services.PatientService;
import hospital.registration.services.UserEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.time.format.DateTimeFormatter;

import static hospital.registration.util.FindErrors.findErrors;
import static hospital.registration.util.GetUserEntity.getUserEntity;

@Controller
public class DoctorsController {
    private final DoctorService doctorService;
    private final PatientService patientService;
    private final UserEntityService userEntityService;
    String redirect = "redirect:/doctors/";
    String id = "{doctor-id}";

    @Autowired
    public DoctorsController(DoctorService doctorService, PatientService patientService, UserEntityService userEntityService) {
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.userEntityService = userEntityService;
    }

    @GetMapping("/doctors")
    public String getDoctorsPage(@RequestParam(required = false, defaultValue = "") String filter, Model model, @PageableDefault(sort = {"id"}, direction = Sort.Direction.ASC) Pageable pageable) {
        model.addAttribute("page", doctorService.page(pageable));
        model.addAttribute("url", "/doctors");
        model.addAttribute("filter", filter);
        model.addAttribute("doctors", doctorService.getAllDoctors());
        model.addAttribute("User", getUserEntity(userEntityService));
        model.addAttribute("count", doctorService.getCountDoctors());
        return "doctors";
    }

    @GetMapping("/doctors/{doctor-id}")
    public String getDoctorPage(Model model, @PathVariable("doctor-id") Integer doctorId) {
        model.addAttribute("dateGraduation", doctorService.getDoctor(doctorId).getDateGraduation().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        model.addAttribute("doctor", doctorService.getDoctor(doctorId));
        model.addAttribute("User", getUserEntity(userEntityService));
        return "doctor";
    }

    @PostMapping("/doctors")
    public String addDoctor(@Valid DoctorForm form, BindingResult bindingResult, RedirectAttributes attributes) {
        if (findErrors(bindingResult, attributes)) return redirect;
        doctorService.addDoctor(form);
        return redirect;
    }

    @PostMapping("/doctors/{doctor-id}/delete")
    public String deleteDoctor(@PathVariable("doctor-id") Integer doctorId) {
        doctorService.deleteDoctor(doctorId);
        return redirect;
    }

    @PostMapping("/doctors/{doctor-id}/update")
    public String updateDoctor(@PathVariable("doctor-id") Integer doctorId, @Valid DoctorForm form, BindingResult bindingResult, RedirectAttributes attributes) {
        if (findErrors(bindingResult, attributes)) return redirect + id;
        doctorService.updateDoctor(doctorId, form);
        return redirect + id;
    }

    @GetMapping("/doctors/{doctor-id}/patients")
    public String getPatientsByDoctor(Model model, @PathVariable("doctor-id") Integer doctorId) {
        model.addAttribute("doctorId", doctorId);
        model.addAttribute("patients", patientService.getPatientsByDoctor(doctorId));
        model.addAttribute("doctor", doctorService.getDoctor(doctorId));
        model.addAttribute("receiptDateFormatter", DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        model.addAttribute("patientsWithoutDoctors", doctorService.getPatientsWithoutDoctors());
        model.addAttribute("User", getUserEntity(userEntityService));
        return "patients_of_doctor";
    }

    @PostMapping("/doctors/{doctor-id}/patients")
    public String addPatientToDoctor(@PathVariable("doctor-id") Integer doctorId, @RequestParam("patientId") Integer patientId) {
        doctorService.addPatientToDoctor(doctorId, patientId);
        return redirect + doctorId + "/patients";
    }

    @PostMapping("/doctors/{doctor-id}/patients/deletePatient")
    public String deletePatientWithoutDoctor(@PathVariable("doctor-id") Integer doctorId, @RequestParam("patientId") Integer patientId) {
        patientService.deletePatient(patientId);
        return redirect + doctorId + "/patients";
    }
}

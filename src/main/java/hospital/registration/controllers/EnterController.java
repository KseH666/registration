package hospital.registration.controllers;

import hospital.registration.services.UserEntityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import static hospital.registration.util.GetUserEntity.getUserEntity;

@RequiredArgsConstructor
@Controller
public class EnterController {
    private final UserEntityService userEntityService;

    @GetMapping("/enter")
    public String getEnterPage(Model model) {
        model.addAttribute("User", getUserEntity(userEntityService));
        return "enter";
    }
}

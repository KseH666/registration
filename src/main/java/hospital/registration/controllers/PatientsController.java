package hospital.registration.controllers;

import hospital.registration.forms.PatientDiseaseForm;
import hospital.registration.forms.PatientForm;
import hospital.registration.services.DiseaseService;
import hospital.registration.services.PatientService;
import hospital.registration.services.RoomService;
import hospital.registration.services.UserEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.time.format.DateTimeFormatter;

import static hospital.registration.util.FindErrors.findErrors;
import static hospital.registration.util.GetUserEntity.getUserEntity;

@Controller
public class PatientsController {
    private final PatientService patientService;
    private final RoomService roomService;
    private final DiseaseService diseaseService;
    private final UserEntityService userEntityService;
    String redirect = "redirect:/patients/";
    String id = "{patient-id}";

    @Autowired
    public PatientsController(PatientService patientService, RoomService roomService, DiseaseService diseaseService, UserEntityService userEntityService) {
        this.patientService = patientService;
        this.roomService = roomService;
        this.diseaseService = diseaseService;
        this.userEntityService = userEntityService;
    }

    @GetMapping("/patients")
    public String getPatientsPage(@RequestParam(required = false, defaultValue = "") String filter, Model model, @PageableDefault(sort = {"patientId"}, direction = Sort.Direction.ASC) Pageable pageable) {
        model.addAttribute("page", patientService.page(pageable));
        model.addAttribute("url", "/patients");
        model.addAttribute("filter", filter);
        model.addAttribute("patients", patientService.getAllPatients());
        model.addAttribute("formatter", DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        model.addAttribute("User", getUserEntity(userEntityService));
        model.addAttribute("count", patientService.getCountPatients());
        return "patients";
    }

    @GetMapping("/patients/{patient-id}")
    public String getPatientPage(Model model, @PathVariable("patient-id") Integer patientId) {
        model.addAttribute("receiptDate", patientService.getPatient(patientId).getReceiptDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        model.addAttribute("dateOfBirth", patientService.getPatient(patientId).getDateOfBirth().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        model.addAttribute("patient", patientService.getPatient(patientId));
        model.addAttribute("rooms", roomService.getAllRooms());
        model.addAttribute("disease", diseaseService.getAllDisease());
        model.addAttribute("User", getUserEntity(userEntityService));
        return "patient";
    }

    @PostMapping("/patients")
    public String addPatient(@Valid PatientForm form, BindingResult bindingResult, RedirectAttributes attributes) {
        if (findErrors(bindingResult, attributes)) return redirect;
        patientService.addPatient(form);
        return redirect;
    }

    @PostMapping("/patients/{patient-id}/delete")
    public String deletePatient(@PathVariable("patient-id") Integer patientId) {
        patientService.deletePatient(patientId);
        return redirect;
    }

    @PostMapping("/patients/{patient-id}/update")
    public String updatePatient(@PathVariable("patient-id") Integer patientId, @Valid PatientForm form, BindingResult bindingResult, RedirectAttributes attributes) {
        if (findErrors(bindingResult, attributes)) return redirect + id;
        patientService.updatePatient(patientId, form);
        return redirect + id;
    }

    @PostMapping("/patients/{patient-id}/deleteDoctor")
    public String deleteDoctor(@PathVariable("patient-id") Integer patientId) {
        Integer doctorId = patientService.getPatient(patientId).getDoctor().getId();
        patientService.deletePatientDoctor(patientId);
        return "redirect:/doctors/" + doctorId + "/patients";
    }

    @PostMapping("/patients/{patient-id}/writeOut")
    public String writeOutPatient(@PathVariable("patient-id") Integer patientId) {
        Integer doctorId = patientService.getPatient(patientId).getDoctor().getId();
        patientService.deletePatient(patientId);
        return "redirect:/doctors/" + doctorId + "/patients";
    }

    @PostMapping("/patients/{patient-id}/updateDisease")
    public String updateDisease(@PathVariable("patient-id") Integer patientId, @Valid PatientDiseaseForm form, BindingResult bindingResult, RedirectAttributes attributes) {
        if (findErrors(bindingResult, attributes)) return redirect + id;
        patientService.updateDiseasePatient(patientId, form);
        return redirect + id;
    }
}

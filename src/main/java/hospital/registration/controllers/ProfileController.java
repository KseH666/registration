package hospital.registration.controllers;

import hospital.registration.services.UserEntityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import static hospital.registration.util.GetUserEntity.getUserEntity;

@RequiredArgsConstructor
@Controller
public class ProfileController {
    private final UserEntityService userEntityService;

    @GetMapping("/profile/{userEntity-id}")
    public String getUserEntityPage(Model model, @PathVariable("userEntity-id") Integer userEntityId) {
        model.addAttribute("User", getUserEntity(userEntityService));
        model.addAttribute("userId", userEntityId);
        return "/profile";
    }
}

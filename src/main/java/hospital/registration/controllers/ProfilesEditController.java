package hospital.registration.controllers;

import hospital.registration.forms.UserEntityForm;
import hospital.registration.services.UserEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import static hospital.registration.util.FindErrors.findErrors;
import static hospital.registration.util.GetUserEntity.getUserEntity;

@Controller
public class ProfilesEditController {
    private final UserEntityService userEntityService;

    @Autowired
    public ProfilesEditController(UserEntityService userEntityService) {
        this.userEntityService = userEntityService;
    }

    @GetMapping("/profiles")
    public String getProfiles(Model model) {
        model.addAttribute("allProfiles", userEntityService.getAllUserEntities());
        model.addAttribute("User", getUserEntity(userEntityService));
        return "profiles";
    }

    @GetMapping("/profiles/{profile-id}")
    public String getProfile(Model model, @PathVariable("profile-id") Integer profileId) {
        model.addAttribute("profile", userEntityService.getUserEntityById(profileId));
        model.addAttribute("User", getUserEntity(userEntityService));
        model.addAttribute("roles", userEntityService.getAllRoles());
        return "profilePage";
    }

    @PostMapping("/profiles/{profile-id}/delete")
    public String deleteProfile(@PathVariable("profile-id") Integer profileId) {
        userEntityService.deleteUserEntity(profileId);
        return "redirect:/profiles/";
    }

    @PostMapping("/profiles/{profile-id}/update")
    public String updateProfile(@PathVariable("profile-id") Integer profileId, @Valid UserEntityForm form, BindingResult bindingResult, RedirectAttributes attributes) {
        if (findErrors(bindingResult, attributes)) return "redirect:/profiles/{profile-id}";
        userEntityService.updateUserEntity(profileId, form);
        return "redirect:/profiles/{profile-id}";
    }
}

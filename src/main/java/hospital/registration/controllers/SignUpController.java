package hospital.registration.controllers;

import hospital.registration.forms.SignUpForm;
import hospital.registration.services.SignUpService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import static hospital.registration.util.FindErrors.findErrors;

@RequiredArgsConstructor
@Controller
@RequestMapping("/signUp")
public class SignUpController {
    private final SignUpService signUpService;

    @GetMapping
    public String getSignUpPage() {
        return "signUp";
    }

    @PostMapping
    public String signUpUser(@Valid SignUpForm form, BindingResult bindingResult, RedirectAttributes attributes) {
        if (findErrors(bindingResult, attributes)) return "redirect:/signUp";
        signUpService.signUpUser(form);
        return "redirect:/signIn";
    }
}

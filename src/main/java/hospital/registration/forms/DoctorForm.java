package hospital.registration.forms;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class DoctorForm {
    @NotEmpty(message = "Пожалуйста, введите фамилию!")
    private String surname;
    @NotEmpty(message = "Пожалуйста, введите имя!")
    private String firstName;
    private String patronymic;
    private String education;
    @JsonFormat(pattern = "DD.MM.YYYY")
    @NotNull(message = "Пожалуйста, введите дату окончания обучения в формате ДД.ММ.ГГГГ")
    private LocalDate dateGraduation;
    private String speciality;
}

package hospital.registration.forms;

import hospital.registration.models.Disease;
import lombok.Data;

@Data
public class PatientDiseaseForm {
    private Disease diseaseCodifier;
    private String diseaseDescription;
    private Integer roomNumber;
    private String treatment;
}

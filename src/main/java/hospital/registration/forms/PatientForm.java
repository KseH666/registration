package hospital.registration.forms;

import com.fasterxml.jackson.annotation.JsonFormat;
import hospital.registration.models.Disease;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class PatientForm {
    private Integer doctorsId;
    @NotEmpty(message = "Пожалуйста, введите фамилию пациента!")
    private String surname;
    @NotEmpty(message = "Пожалуйста, введите имя пациента!")
    private String firstName;
    private String patronymic;
    @JsonFormat(pattern = "DD.MM.YYYY")
    @NotNull(message = "Пожалуйста, введите дату рождения в формате ДД.ММ.ГГГГ")
    private LocalDate dateOfBirth;
    private Integer age;
    private String sex;
    @JsonFormat(pattern = "DD.MM.YYYY")
    @NotNull(message = "Пожалуйста, введите дату поступления в формате ДД.ММ.ГГГГ")
    private LocalDate receiptDate;
    private Disease diseaseCodifier;
    private String diseaseDescription;
    private Integer roomNumber;
    private String treatment;
}

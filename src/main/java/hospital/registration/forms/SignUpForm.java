package hospital.registration.forms;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class SignUpForm {
    @NotEmpty(message = "Пожалуйста, введите имя")
    private String name;
    @NotEmpty(message = "Пожалуйста, введите почту")
    private String email;
    @Size(min = 6, max = 30, message = "Пароль должен содержать от 6 до 30 символов!")
    private String password;
}

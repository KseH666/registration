package hospital.registration.forms;

import hospital.registration.util.Role;
import lombok.Data;

@Data
public class UserEntityForm {
    private Role role;
    private String name;
    private boolean verified;
}

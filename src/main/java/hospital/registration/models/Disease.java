package hospital.registration.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "disease_codifier")
public class Disease {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String code;
    private String description;
    @OneToMany(mappedBy = "diseaseCodifier")
    private List<Patient> patients;
}

package hospital.registration.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "patients")
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer patientId;
    private String surname;
    private String firstName;
    private String patronymic;
    private LocalDate dateOfBirth;
    private Integer age;
    private String sex;
    private LocalDate receiptDate;
    @ManyToOne
    @JoinColumn(name = "disease_codifier")
    private Disease diseaseCodifier;
    private String diseaseDescription;
    private Integer roomNumber;
    private String treatment;
    @ManyToOne
    @JoinColumn(name = "doctors_id")
    private Doctor doctor;
}

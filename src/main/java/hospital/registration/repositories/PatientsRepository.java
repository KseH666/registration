package hospital.registration.repositories;

import hospital.registration.models.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PatientsRepository extends JpaRepository<Patient, Integer> {
    List<Patient> findAllByDoctorId(Integer id);

    List<Patient> findAllByDoctorIsNull();
}

package hospital.registration.services;

import hospital.registration.models.Disease;

import java.util.List;

public interface DiseaseService {
    List<Disease> getAllDisease();
}

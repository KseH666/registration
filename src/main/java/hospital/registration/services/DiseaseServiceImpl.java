package hospital.registration.services;

import hospital.registration.models.Disease;
import hospital.registration.repositories.DiseaseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class DiseaseServiceImpl implements DiseaseService {
    private final DiseaseRepository diseaseRepository;

    @Override
    public List<Disease> getAllDisease() {
        return diseaseRepository.findAll();
    }
}

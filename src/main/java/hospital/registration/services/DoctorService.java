package hospital.registration.services;

import hospital.registration.forms.DoctorForm;
import hospital.registration.models.Doctor;
import hospital.registration.models.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DoctorService {
    void addDoctor(DoctorForm form);

    List<Doctor> getAllDoctors();

    void deleteDoctor(Integer doctorId);

    Doctor getDoctor(Integer doctorId);

    List<Patient> getPatientsWithoutDoctors();

    void addPatientToDoctor(Integer patientId, Integer doctorId);

    void updateDoctor(Integer doctorId, DoctorForm form);

    Integer getCountDoctors();

    Page<Doctor> page(Pageable pageable);
}

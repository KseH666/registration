package hospital.registration.services;

import hospital.registration.forms.DoctorForm;
import hospital.registration.models.Doctor;
import hospital.registration.models.Patient;
import hospital.registration.repositories.DoctorsRepository;
import hospital.registration.repositories.PatientsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@Component
public class DoctorServiceImpl implements DoctorService {
    private final DoctorsRepository doctorsRepository;
    private final PatientsRepository patientsRepository;

    @Override
    public void addDoctor(DoctorForm form) {
        Doctor doctor = Doctor.builder()
                .surname(form.getSurname())
                .firstName(form.getFirstName())
                .patronymic(form.getPatronymic())
                .education(form.getEducation())
                .dateGraduation(LocalDate.parse(form.getDateGraduation().toString()))
                .speciality(form.getSpeciality())
                .build();
        doctorsRepository.save(doctor);
    }

    @Override
    public List<Doctor> getAllDoctors() {
        return doctorsRepository.findAll();
    }

    @Override
    public void deleteDoctor(Integer doctorId) {
        doctorsRepository.deleteById(doctorId);
    }

    @Override
    public Doctor getDoctor(Integer doctorId) {
        return doctorsRepository.getById(doctorId);
    }


    @Override
    public List<Patient> getPatientsWithoutDoctors() {
        return patientsRepository.findAllByDoctorIsNull();
    }

    @Override
    public void addPatientToDoctor(Integer doctorId, Integer patientId) {
        Patient patient = patientsRepository.getById(patientId);
        Doctor doctor = doctorsRepository.getById(doctorId);
        patient.setDoctor(doctor);
        patientsRepository.save(patient);
    }

    @Override
    public void updateDoctor(Integer doctorId, DoctorForm form) {
        Doctor oldDoctor = doctorsRepository.getById(doctorId);
        oldDoctor.setSurname(form.getSurname());
        oldDoctor.setFirstName(form.getFirstName());
        oldDoctor.setPatronymic(form.getPatronymic());
        oldDoctor.setEducation(form.getEducation());
        oldDoctor.setDateGraduation(LocalDate.parse(form.getDateGraduation().toString()));
        oldDoctor.setSpeciality(form.getSpeciality());
        doctorsRepository.save(oldDoctor);
    }

    @Override
    public Integer getCountDoctors() {
        return doctorsRepository.findAll().size();
    }

    @Override
    public Page<Doctor> page(Pageable pageable) {
        return doctorsRepository.findAll(pageable);
    }
}

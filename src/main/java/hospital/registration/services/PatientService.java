package hospital.registration.services;

import hospital.registration.forms.PatientDiseaseForm;
import hospital.registration.forms.PatientForm;
import hospital.registration.models.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PatientService {
    void addPatient(PatientForm form);

    List<Patient> getAllPatients();

    void deletePatient(Integer patientId);

    List<Patient> getPatientsByDoctor(Integer doctorId);

    Patient getPatient(Integer patientId);

    void updatePatient(Integer patientId, PatientForm form);

    void deletePatientDoctor(Integer patientId);

    void updateDiseasePatient(Integer patientId, PatientDiseaseForm form);

    Integer getCountPatients();

    Page<Patient> page(Pageable pageable);
}

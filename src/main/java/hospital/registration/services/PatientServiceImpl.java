package hospital.registration.services;

import hospital.registration.forms.PatientDiseaseForm;
import hospital.registration.forms.PatientForm;
import hospital.registration.models.Patient;
import hospital.registration.repositories.PatientsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@Component
public class PatientServiceImpl implements PatientService {
    private final PatientsRepository patientsRepository;

    @Override
    public void addPatient(PatientForm form) {
        Patient patient = Patient.builder()
                .surname(form.getSurname())
                .firstName(form.getFirstName())
                .patronymic(form.getPatronymic())
                .sex(form.getSex())
                .dateOfBirth(form.getDateOfBirth())
                .age((int) ((System.currentTimeMillis() - ((form.getDateOfBirth().toEpochDay() + 1) * 86400000)) / 31536000000L))
                .receiptDate(LocalDate.parse(form.getReceiptDate().toString()))
                .diseaseCodifier(null).roomNumber(null).diseaseDescription(null).treatment(null).doctor(null)
                .build();
        patientsRepository.save(patient);
    }

    @Override
    public List<Patient> getAllPatients() {
        return patientsRepository.findAll();
    }

    @Override
    public void deletePatient(Integer patientId) {
        patientsRepository.deleteById(patientId);
    }

    @Override
    public List<Patient> getPatientsByDoctor(Integer doctorId) {
        return patientsRepository.findAllByDoctorId(doctorId);
    }

    @Override
    public Patient getPatient(Integer patientId) {
        return patientsRepository.getById(patientId);
    }

    @Override
    public void updatePatient(Integer patientId, PatientForm form) {
        Patient oldPatient = patientsRepository.getById(patientId);
        oldPatient.setSurname(form.getSurname());
        oldPatient.setFirstName(form.getFirstName());
        oldPatient.setPatronymic(form.getPatronymic());
        oldPatient.setSex(form.getSex());
        oldPatient.setDateOfBirth(LocalDate.parse(form.getDateOfBirth().toString()));
        oldPatient.setAge((int) ((System.currentTimeMillis() - ((oldPatient.getDateOfBirth().toEpochDay() + 1) * 86400000)) / 31536000000L));
        oldPatient.setReceiptDate(LocalDate.parse(form.getReceiptDate().toString()));
        patientsRepository.save(oldPatient);
    }

    @Override
    public void deletePatientDoctor(Integer patientId) {
        Patient oldPatient = patientsRepository.getById(patientId);
        oldPatient.setDoctor(null);
        patientsRepository.save(oldPatient);
    }

    @Override
    public void updateDiseasePatient(Integer patientId, PatientDiseaseForm form) {
        Patient oldPatient = patientsRepository.getById(patientId);
        oldPatient.setDiseaseCodifier(form.getDiseaseCodifier());
        oldPatient.setDiseaseDescription(form.getDiseaseDescription());
        oldPatient.setRoomNumber(form.getRoomNumber());
        oldPatient.setTreatment(form.getTreatment());
        patientsRepository.save(oldPatient);
    }

    @Override
    public Integer getCountPatients() {
        return patientsRepository.findAll().size();
    }

    @Override
    public Page<Patient> page(Pageable pageable) {
        return patientsRepository.findAll(pageable);
    }
}

package hospital.registration.services;

import hospital.registration.models.Room;

import java.util.List;

public interface RoomService {
    List<Room> getAllRooms();
}

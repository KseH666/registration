package hospital.registration.services;

import hospital.registration.models.Room;
import hospital.registration.repositories.RoomsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class RoomServiceImpl implements RoomService {
    private final RoomsRepository roomsRepository;

    @Override
    public List<Room> getAllRooms() {
        return roomsRepository.findAll();
    }
}

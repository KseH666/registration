package hospital.registration.services;

import hospital.registration.forms.SignUpForm;

public interface SignUpService {
    void signUpUser(SignUpForm form);
}

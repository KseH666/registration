package hospital.registration.services;

import hospital.registration.forms.SignUpForm;
import hospital.registration.models.UserEntity;
import hospital.registration.repositories.UsersEntityRepository;
import hospital.registration.util.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;
    private final UsersEntityRepository usersEntityRepository;

    @Override
    public void signUpUser(SignUpForm form) {
        UserEntity userEntity = UserEntity.builder()
                .name(form.getName())
                .email(form.getEmail())
                .role(Role.USER)
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .verified(false)
                .build();
        usersEntityRepository.save(userEntity);
    }
}

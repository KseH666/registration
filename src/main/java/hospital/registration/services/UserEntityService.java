package hospital.registration.services;

import hospital.registration.forms.UserEntityForm;
import hospital.registration.models.UserEntity;
import hospital.registration.util.Role;

import java.util.List;
import java.util.Optional;

public interface UserEntityService {
    List<UserEntity> getAllUserEntities();

    UserEntity getUserEntityById(Integer userEntityId);

    void deleteUserEntity(Integer userEntityId);

    void updateUserEntity(Integer userEntityId, UserEntityForm form);

    Optional<UserEntity> getUserEntityByMail(String mail);

    List<Role> getAllRoles();
}

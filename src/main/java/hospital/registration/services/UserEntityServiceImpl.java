package hospital.registration.services;

import hospital.registration.forms.UserEntityForm;
import hospital.registration.models.UserEntity;
import hospital.registration.repositories.UsersEntityRepository;
import hospital.registration.util.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Component
public class UserEntityServiceImpl implements UserEntityService {
    private final UsersEntityRepository usersEntityRepository;

    @Override
    public List<UserEntity> getAllUserEntities() {
        return usersEntityRepository.findAll();
    }

    @Override
    public UserEntity getUserEntityById(Integer userEntityId) {
        return usersEntityRepository.getById(userEntityId);
    }

    @Override
    public void deleteUserEntity(Integer userEntityId) {
        usersEntityRepository.deleteById(userEntityId);
    }

    @Override
    public void updateUserEntity(Integer userEntityId, UserEntityForm form) {
        UserEntity oldUserEntity = usersEntityRepository.getById(userEntityId);
        oldUserEntity.setVerified(form.isVerified());
        oldUserEntity.setRole(form.getRole());
        oldUserEntity.setName(form.getName());
        usersEntityRepository.save(oldUserEntity);
    }

    @Override
    public Optional<UserEntity> getUserEntityByMail(String mail) {
        return usersEntityRepository.findByEmail(mail);
    }

    @Override
    public List<Role> getAllRoles() {
        List<Role> roles = new ArrayList<>();
        Collections.addAll(roles, Role.values());
        return roles;
    }
}

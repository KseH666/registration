package hospital.registration.util;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

public class FindErrors {
    public static boolean findErrors(BindingResult bindingResult, RedirectAttributes attributes) {
        if (bindingResult.hasErrors()) {
            List<ObjectError> errors = bindingResult.getAllErrors();
            for (int a = 0; a < bindingResult.getAllErrors().size(); a++) {
                attributes.addFlashAttribute("error" + a, errors.get(a).getDefaultMessage());
            }
            return true;
        }
        return false;
    }
}

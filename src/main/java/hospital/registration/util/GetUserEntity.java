package hospital.registration.util;

import hospital.registration.models.UserEntity;
import hospital.registration.services.UserEntityService;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class GetUserEntity {
    public static Optional<UserEntity> getUserEntity(UserEntityService userEntityService) {
        return userEntityService.getUserEntityByMail(SecurityContextHolder.getContext().getAuthentication().getName());
    }
}

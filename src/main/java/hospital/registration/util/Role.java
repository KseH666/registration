package hospital.registration.util;

public enum Role {
    ADMIN, USER
}
create table doctors
(
    id              serial primary key,
    surname         varchar(255),
    first_name      varchar(255),
    patronymic      varchar(255),
    education       varchar(255),
    date_graduation date,
    speciality      varchar(255)
);
create table disease_codifier
(
    code        varchar(5) primary key,
    description varchar(255)
);
insert into disease_codifier(code, description)
values ('000', 'Не классифицирована'),
       ('A00', 'Холера'),
       ('A01', 'Тиф и паратиф'),
       ('A02', 'Другие сальмонеллезные инфекции'),
       ('A03', 'Шигеллез'),
       ('A04', 'Другие бактериальные кишечные инфекции'),
       ('A05', 'Другие бактериальные пищевые отравления'),
       ('A06', 'Амебиаз'),
       ('A07', 'Другие протозойные кишечные болезни'),
       ('A08', 'Вирусные и другие уточненные кишечные инфекции'),
       ('A09', 'Диарея и гастроэнтерит предположительно инфекционного происхождения');
create table rooms
(
    number integer primary key
);
insert into rooms
values (0),
       (1),
       (2);
create table sexes
(
    sex varchar(13) primary key
);
insert into sexes
values ('Мужской'),
       ('Женский'),
       ('Не установлен');
create table patients
(
    patient_id          serial primary key,
    doctors_id          integer,
    surname             varchar(255),
    first_name          varchar(255),
    patronymic          varchar(255),
    date_of_birth       date,
    age                 integer,
    sex                 varchar(13),
    receipt_date        date,
    disease_codifier    varchar(5) default ('000'),
    disease_description text,
    room_number         integer,
    treatment           text,
    foreign key (doctors_id) references doctors (id),
    foreign key (sex) references sexes (sex),
    foreign key (disease_codifier) references disease_codifier (code),
    foreign key (room_number) references rooms (number)
);
create table account
(
    id            serial primary key,
    name          varchar(50),
    email         varchar(255) unique,
    role          varchar(255),
    hash_password varchar(255),
    verified      boolean
);
insert into account
values (0, 'admin', '2@2', 'ADMIN', '$2a$10$qw6r5OZ3u8lm4ji3GmHPl.r45EhPs8DzsnaxjCz2fkkcBWMNnqRHu', true);
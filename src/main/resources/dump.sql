insert into doctors (surname, first_name, patronymic, education, date_graduation, speciality)
values ('Иванов', 'Иван', 'Иванович', 'СПБГМУ', '2016-01-01', 'Гастроэнтеролог'),
       ('Петров', 'Пётр', 'Петрович', 'СПБГПУм', '2010-07-03', 'Эндокринилог'),
       ('Димитро', 'Валенсо', 'Ихитар', 'LAU Empi', '2020-08-12', 'Узист');

insert into patients (surname, first_name, patronymic, date_of_birth, age, sex, receipt_date)
values ('Алексеев', 'Алексей', 'Владимирович', '1990-01-03',
        date_part('year', age(timestamp 'now()', timestamp '1990-01-03')), 'Мужской', '2021-08-12'),
       ('Симоньян', 'Дмитрий', 'Васкелович', '1975-03-01',
        date_part('year', age(timestamp 'now()', timestamp '1975-03-01')), 'Мужской', '2021-07-12'),
       ('Гарин', 'Геннадий', 'Дмитриевич', '1971-12-12',
        date_part('year', age(timestamp 'now()', timestamp '1971-12-12')), 'Мужской', '2021-06-12'),
       ('Верескин', 'Игорь', 'Петрович', '1987-07-04',
        date_part('year', age(timestamp 'now()', timestamp '1987-07-04')), 'Мужской', '2021-08-12'),
       ('Кастель', 'Антон', 'Игоревич', '1956-03-05', date_part('year', age(timestamp 'now()', timestamp '1956-03-05')),
        'Мужской', '2021-05-12'),
       ('Васницов', 'Фёдор', 'Артёмович', '1986-04-02',
        date_part('year', age(timestamp 'now()', timestamp '1986-04-02')), 'Мужской', '2021-07-12'),
       ('Веслов', 'Александр', 'Иванович', '1995-04-01',
        date_part('year', age(timestamp 'now()', timestamp '1995-04-01')), 'Мужской', '2021-03-12'),
       ('Важена', 'Екатерина', 'Александровна', '1948-02-03',
        date_part('year', age(timestamp 'now()', timestamp '1948-02-03')), 'Женский', '2021-01-12');

insert into account(name, email, role, hash_password, verified)
values ('Пересмыслов Иван', 'ivan@ya.ru', 'USER', '$2a$10$qw6r5OZ3u8lm4ji3GmHPl.r45EhPs8DzsnaxjCz2fkkcBWMNnqRHu', true),
       ('Вилодин Виктор', 'ivictor@mail.ru', 'USER', '$2a$10$qw6r5OZ3u8lm4ji3GmHPl.r45EhPs8DzsnaxjCz2fkkcBWMNnqRHu', false),
       ('Постумышева Екатерина', 'ekat@gmail.com', 'USER', '$2a$10$qw6r5OZ3u8lm4ji3GmHPl.r45EhPs8DzsnaxjCz2fkkcBWMNnqRHu', false);

drop table patients;
drop table doctors;
drop table disease_codifier;
drop table rooms;
drop table account;
drop table sexes;

select *
from doctors;
select *
from patients;
select *
from account;
select *
from rooms;
select *
from disease_codifier;
select *
from sexes;

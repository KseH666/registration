package hospital.registration;

import hospital.registration.models.Doctor;
import hospital.registration.models.Patient;
import hospital.registration.services.DoctorService;
import hospital.registration.services.PatientService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
class RegistrationApplicationTests {
    @Autowired
    private DoctorService service;
    @Autowired
    private PatientService patientService;
    @Test
    public void testGetByIdFromDb() {
        Doctor doctor = service.getDoctor(1);
        assertThat(doctor.getId()).isEqualTo(1);
    }
    @Test
    public void testGetByPatientIdFromDb() {
        Patient patient = patientService.getPatient(1);
        assertThat(patient.getPatientId()).isEqualTo(1);
    }
}

package hospital.registration;

import static org.assertj.core.api.Assertions.assertThat;

import hospital.registration.controllers.DoctorsController;
import hospital.registration.controllers.PatientsController;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SmokeTest {

    @Autowired
    private DoctorsController controller;
    @Autowired
    private PatientsController patientsController;

    @Test
    public void contextLoads() {
        assertThat(controller).isNotNull();
    }
    @Test
    public void patientContextLoads() {
        assertThat(patientsController).isNotNull();
    }
}
